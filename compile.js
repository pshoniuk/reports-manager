"use strict";
require("colors");
const ARCH = { x64: 1, ia32: 0 },
	PLATFORMS = ["win!x64",  "win!ia32", "linux!x64", "linux!ia32", "mac!x64", "mac!x32"],
	os = require('os'),
	fs = require("fs"),
	builder = require("electron-builder"),
	path = require("path"),
	inquirer = require('inquirer'),
	Platform = builder.Platform,
	currentVersion = JSON.parse(fs.readFileSync('./app/package.json')).version,
	iconDir = path.resolve("./app/app-icons"),
	spinner = path.resolve("./app/spinner.gif"),
	productName = JSON.parse(fs.readFileSync('./app/package.json')).name;

let platformsChoices = [];
inquirer.prompt([
	{
		type: 'checkbox',
		message: 'Select platforms',
		name: 'platforms',
		choices: (platformsChoices = [
			{
				name: 'Windows_x32',
				checked: os.platform() === 'win32'
			},
			{
				name: 'Windows_x64',
				checked: os.platform() === 'win32'
			},
			{
				name: 'Linux_x32',
				checked: os.platform() === 'linux'
			},
			{
				name: 'Linux_x64',
				checked: os.platform() === 'linux'
			},
			{
				name: 'MacOS_x32',
				checked: os.platform() === 'darwin'
			},
			{
				name: 'MacOS_x64',
				checked: os.platform() === 'darwin'
			}
		]),

		validate(answer) {
			if (answer.length < 1) {
				return 'You must choose at least one target.';
			}
			return true;
		}
	},
	{
		type: 'input',
		message: `What is the new version?(Current: ${currentVersion})?`,
		name: 'version',

		validate(newVersion) {
			const normalizeVersionParts = (newVersion || '')
				.trim()
				.split('.')
				.map(n => +n)
				.filter(n => !isNaN(n));

			if (normalizeVersionParts.length !== 3) return 'The version number is not correct.';
			return true;
		}
	}
]).then(function (answers) {
	const platforms = answers.platforms
		.map(platformName => platformsChoices.findIndex(choice => choice.name === platformName))
		.map(platformIndex => PLATFORMS[platformIndex]);

	writeNewVersion(answers.version);
	compile(platforms, answers.version);
});


function writeNewVersion(newVersion) {
	const appPackagePath = './app/package.json',
		selfPackagePath = './app/package.json',
		appPackage = JSON.parse(fs.readFileSync(appPackagePath)),
		selfPackage = JSON.parse(fs.readFileSync(selfPackagePath));

	appPackage.version = newVersion;
	selfPackage.version = newVersion;

	fs.writeFileSync(appPackagePath, JSON.stringify(appPackage, null, 2));
	fs.writeFileSync(selfPackagePath, JSON.stringify(selfPackage, null, 2));
}

function compile(platforms, version) {
	require("rmdir")("./bin", (err) => {
		if (err) console.error(err);

		platforms.forEach((platform) => {
			const conf = platform.split("!");

			setTimeout(() => {
				builder.build({
					targets: Platform.fromString(conf[0]).createTarget(null, ARCH[conf[1]]),
					devMetadata: {
						build: {
							appId: "Amber.uploader2017",
							copyright: "Copyright © 2017 V. Pshonyuk",
							directories: {
								app: "./app",
								output: `./bin/${conf[0]}_${conf[1]}`,
								buildResources: `./bin/${conf[0]}_${conf[1]}`
							},
							linux: {
								category: "design",
								executableName: productName.toLowerCase() + version
							},
							win: {
								icon: path.join(iconDir, "icon.ico"),
								target: "nsis",
								publisherName: productName.toLowerCase() + '_' + version
							},
							nsis: {
								oneClick: false,
								runAfterFinish: false
							},
							squirrelWindows: {
								icon: path.join(iconDir, "icon.ico"),
								msi: true,
								loadingGif: spinner
							},
							mac: {
								// icon: path.join(iconDir, "icon.ico")
							}
						}
					}
				})
					.then(() => {
						console.log(`Success build for ${platform}!`.green);
					})
					.catch((error) => {
						console.error(`Error build for ${platform}. ${error}`.red);
					});
			}, 0);
		});
	});
}
