import * as jQuery from 'jquery';
import * as moment from 'moment';
import { generateColorHash } from './utils';
import { ReportsFilesManager, IReportItemData } from './ReportsFilesManager';
import { ReportEditor } from './ReportEditor';

export function displayCreatedDate(timestamp: number): string {
  moment.locale('uk');
  return moment(timestamp).format('ll');
}

export class ReportsList {
  public static LIST_SELECTOR: string = '#reports .reports-list';
  public static REFRESH_SELECTOR: string = '#reports .refresh-list';

  private reportsFilesManager: ReportsFilesManager;
  private reportEditor: ReportEditor;

  constructor() {
    this.reportsFilesManager = new ReportsFilesManager();
    this.reportEditor = new ReportEditor();

    this.render = this.render.bind(this);
    this.reportsFilesManager.listSubject.subscribe(this.render);

    this.onSelectItem = this.onSelectItem.bind(this);
    jQuery(this.getContainer()).on('click', '.list-group-item', this.onSelectItem);

    this.onRefreshClick = this.onRefreshClick.bind(this);
    jQuery(ReportsList.REFRESH_SELECTOR).on('click', this.onRefreshClick);
  }

  public render(reportsList: IReportItemData[]): void {
    const container: HTMLElement = this.getContainer(),
      $activeItem = jQuery('.list-group-item.active'),
      activeFD = $activeItem.length && $activeItem.data('fd');

    let newContent: string = '';

    reportsList
      .sort((a, b) => {
        if (!a.createdAt) return -1;
        if (!b.createdAt) return 1;
        if (a.createdAt > b.createdAt) return -1;
        if (a.createdAt < b.createdAt) return 1;
        return 0;
      })
      .forEach((reportData: IReportItemData, i) => {
        newContent += `
        <a href="#" 
          class="list-group-item"
          data-id="${i}"
          data-fd="${reportData.fd}"
        >
          <div class="author-label" style="background-color: ${generateColorHash(reportData.author)}"></div>
          <div class="content">
            <div class="name">
              ${reportData.name}
            </div>
            <div class="time">
              ${!reportData.createdAt ? '' : `
              <span class="badge badge-default">
                ${displayCreatedDate(reportData.createdAt)}
              </span>
              `}
            </div>
          </div>
        </a>`;
      });

    container.innerHTML = newContent;

    if (activeFD) {
      const $futureActiveItem = jQuery(this.getContainer()).find(`[data-fd="${activeFD}"]`);
      $futureActiveItem.addClass('active');
    }
  }

  public getContainer(): HTMLElement {
    return <HTMLElement>document.querySelector(ReportsList.LIST_SELECTOR);
  }

  private onSelectItem(e: Event): void {
    e.preventDefault();
    const $item = jQuery(e.target).closest('.list-group-item'),
      id: string = $item.data('id');

    jQuery(this.getContainer())
      .find('.list-group-item')
      .not($item)
      .removeClass('active');

    $item.toggleClass('active');
    const reportData: IReportItemData = $item.hasClass('active') ?
      this.reportsFilesManager.listSubject.getValue()[id] : null;

    this.reportEditor.editReport(reportData);
  }

  private onRefreshClick(e: Event) {
    e.preventDefault();
    this.reportsFilesManager.updateList();
  }
}
