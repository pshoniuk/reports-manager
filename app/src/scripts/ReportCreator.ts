import { ReportsFilesManager, IReportItemData } from './ReportsFilesManager';
const BASIC_TEMPLATE = `<p style=\"text-align: center;\"><span style=\"font-size: 12pt;\"><strong>Звіт про проведену роботу</strong></span></p>\n<p style=\"text-align: center;\"><span style=\"font-size: 12pt;\"><strong>головного спеціаліста віддділу ЕТН та ЯМП</strong></span></p>\n<p style=\"text-align: center;\"><span style=\"font-size: 12pt;\">&nbsp;</span></p>\n<table style=\"height: 86px; width: 100%; border-color: #000000;\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td style=\"width: 73px; text-align: center;\"><span style=\"font-size: 12pt;\"><strong>№ п/п</strong></span></td>\n<td style=\"width: 573px; text-align: center;\"><span style=\"font-size: 12pt;\"><strong>Найменування показника&nbsp;</strong></span></td>\n<td style=\"width: 159px; text-align: center;\"><span style=\"font-size: 12pt;\"><strong>Значення показника</strong></span></td>\n</tr>\n<tr>\n<td style=\"width: 73px; text-align: center;\"><span style=\"font-size: 12pt;\"><strong>&nbsp;1</strong></span></td>\n<td style=\"width: 573px; text-align: center;\"><span style=\"font-size: 12pt;\"><strong>2&nbsp;</strong></span></td>\n<td style=\"width: 159px; text-align: center;\"><span style=\"font-size: 12pt;\"><strong>3&nbsp;</strong></span></td>\n</tr>\n<tr>\n<td style=\"width: 73px; text-align: center;\"><span style=\"font-size: 12pt;\">&nbsp;1.</span></td>\n<td style=\"width: 573px;\"><span style=\"font-size: 12pt;\">&nbsp;</span></td>\n<td style=\"width: 159px;\"><span style=\"font-size: 12pt;\">&nbsp;</span></td>\n</tr>\n</tbody>\n</table>`;


export class ReportCreator {
  private static REPORT_NAME_INPUT_SELECTOR: string = '#reports .new-report-data .report-name input';
  private static REPORT_ADD_BUTTON_SELECTOR: string = '#reports .new-report-data .create-report button';
  private static BASIC_TEMPLATE_NAME: string = 'Базовий шаблон';

  private reportsFilesManager: ReportsFilesManager;

  constructor() {
    this.reportsFilesManager = new ReportsFilesManager();

    const reportAddBtn: HTMLElement = <HTMLElement>document.querySelector(ReportCreator.REPORT_ADD_BUTTON_SELECTOR);
    this.onAddClick = this.onAddClick.bind(this);
    reportAddBtn.addEventListener('click', this.onAddClick, false);
  }

  private onAddClick(e: Event): void {
    e.preventDefault();

    const reportsList: IReportItemData[] = this.reportsFilesManager.listSubject.getValue(),
      reportNameInput: HTMLInputElement = <HTMLInputElement>document.querySelector(ReportCreator.REPORT_NAME_INPUT_SELECTOR),
      inputValue: string = reportNameInput.value,
      reportName: string = inputValue ? inputValue : `Звіт ${reportsList.length}`,
      fd: string = ReportsFilesManager.getUniqueFd(),
      body: string = this.getBasicTemplate();

    this.reportsFilesManager.add({
      body: body,
      name: reportName,
      fd: fd,
      createdAt: Date.now(),
      author: ''
    });
  }

  public getBasicTemplate(): string {
    const reportsList: IReportItemData[] = this.reportsFilesManager.listSubject.getValue(),
      indexBasicTemplate: number = reportsList.findIndex((reportData: IReportItemData) => reportData.name === ReportCreator.BASIC_TEMPLATE_NAME);

    if (indexBasicTemplate >= 0) {
      return reportsList[indexBasicTemplate].body;
    } else if (reportsList[0]) {
      reportsList[0].body;
    }

    this.createBasicTemplate();
    return BASIC_TEMPLATE;
  }

  private createBasicTemplate(): void {
    this.reportsFilesManager.add({
      fd: `0${ReportsFilesManager.fileExt}`,
      body: BASIC_TEMPLATE,
      name: ReportCreator.BASIC_TEMPLATE_NAME,
      createdAt: 0,
      author: ''
    })
  }
}
