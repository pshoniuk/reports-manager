import { ReportsList } from './ReportsList';
import { ReportCreator } from './ReportCreator';
import { ReportEditor } from './ReportEditor';
import { Statistic } from './Statistic';
import { Gallery } from './Gallery';

class Application {
  public reportsList: ReportsList;
  public reportCreator: ReportCreator;
  public reportEditor: ReportEditor;
  public gallery: Gallery;
  public statistic: Statistic;

  constructor() {
    this.reportEditor = new ReportEditor();
    this.reportsList = new ReportsList();
    this.reportCreator = new ReportCreator();
    this.reportCreator.getBasicTemplate();
    this.gallery = new Gallery('#multimedia');
    this.statistic = new Statistic();
  }
}


export const app: Application = new Application();
