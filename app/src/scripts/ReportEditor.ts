import { ReportsFilesManager, IReportItemData } from './ReportsFilesManager';
import * as moment from 'moment';

export class ReportEditor {
  private static instance: ReportEditor;
  public static CONTAINER_SELECTOR: string = '#report-editor';
  public static NAME_SELECTOR: string = '#report-editor .report-name input';
  public static AUTHOR_SELECTOR: string = '#report-editor .report-author input';
  public static BODY_SELECTOR: string = '#report-editor .report-body textarea';
  public static SAVE_SELECTOR: string = '#report-editor .manage-buttons .save';
  public static CANCEL_SELECTOR: string = '#report-editor .manage-buttons .cancel';
  public static REMOVE_SELECTOR: string = '#report-editor .manage-buttons .remove';

  private reportsFilesManager: ReportsFilesManager;
  public editedReportData: IReportItemData;
  public tinyMCE: any;

  constructor() {
    if (ReportEditor.instance) return ReportEditor.instance;
    ReportEditor.instance = this;

    this.reportsFilesManager = new ReportsFilesManager();
    this
      .initTinyMCE()
      .then((editors) => {
        this.tinyMCE = editors[0];
        this.bindManageActions();
      })
      .catch((err: Error) => {
        console.error(err);
      });
  }

  private initTinyMCE(): Promise<any> {
    let fontSizeFormats = '';

    for (let i = 8; i <= 48; i += 2) {
      fontSizeFormats += `${i}pt `;
    }

    return (<any>window).tinymce.init({
      selector: ReportEditor.BODY_SELECTOR,
      height: 500,
      theme: 'modern',
      language: 'uk_UA',
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking table contextmenu directionality',
        'emoticons paste textcolor colorpicker textpattern imagetools codesample toc help'
      ],
      fontsize_formats: fontSizeFormats.trim(),
      toolbar1: 'undo redo | sizeselect | bold italic | fontselect | fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help'
    });
  }

  private bindManageActions(): void {
    const saveBtn: HTMLButtonElement = <HTMLButtonElement>document.querySelector(ReportEditor.SAVE_SELECTOR);
    this.saveClick = this.saveClick.bind(this);
    saveBtn.addEventListener('click', this.saveClick, false);

    const cancelBtn: HTMLButtonElement = <HTMLButtonElement>document.querySelector(ReportEditor.CANCEL_SELECTOR);
    this.cancelClick = this.cancelClick.bind(this);
    cancelBtn.addEventListener('click', this.cancelClick, false);

    const removeBtn: HTMLButtonElement = <HTMLButtonElement>document.querySelector(ReportEditor.REMOVE_SELECTOR);
    this.removeClick = this.removeClick.bind(this);
    removeBtn.addEventListener('click', this.removeClick, false);
  }

  private saveClick(e: Event): void {
    e.preventDefault();

    this.reportsFilesManager.update({
      ...this.editedReportData,
      createdAt: +(new Date((<HTMLInputElement>document.getElementById('sdate')).value)) || Date.now(),
      body: (<any>window).tinyMCE.activeEditor.getContent(),
      name: this.getNameInput().value,
      author: this.getAuthorInput().value
    })
  }

  private cancelClick(e: Event): void {
    e.preventDefault();

    this.editReport(this.editedReportData);
  }

  private removeClick(e: Event): void {
    e.preventDefault();

    const editedReportData = this.editedReportData;
    this.editReport(null);
    this.reportsFilesManager.remove(editedReportData);
  }

  public editReport(reportData: IReportItemData): void {
    this.editedReportData = reportData;
    if (!reportData) return this.hideEditor();

    this.showEditor();
    this.getNameInput().value = reportData.name;
    this.getAuthorInput().value = reportData.author;
    (<HTMLInputElement>document.getElementById('sdate')).value = moment(reportData.createdAt).format('YYYY-MM-DD');
    this.tinyMCE.setContent(reportData.body);
  }

  private showEditor(): void {
    this.getContainer().style.display = '';
  }

  private hideEditor(): void {
    this.getContainer().style.display = 'none';
  }

  private getContainer(): HTMLElement {
    return <HTMLElement>document.querySelector(ReportEditor.CONTAINER_SELECTOR);
  }

  private getNameInput(): HTMLInputElement {
    return <HTMLInputElement>document.querySelector(ReportEditor.NAME_SELECTOR);
  }

  private getAuthorInput(): HTMLInputElement {
    return <HTMLInputElement>document.querySelector(ReportEditor.AUTHOR_SELECTOR);
  }
}
