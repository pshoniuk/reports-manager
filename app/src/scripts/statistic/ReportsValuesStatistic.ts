import * as $ from 'jquery';
import * as moment from 'moment';
import { groupBy, unionBy } from 'lodash';
import { IReportItemData, ReportsFilesManager } from '../ReportsFilesManager';

export function displayCreatedDate(timestamp: number): string {
  moment.locale('uk');
  return moment(timestamp).format('ll');
}

export class ReportsValuesStatistic {
  public static CONTAINER: HTMLElement = <HTMLElement>document.querySelector('#reports-values-statistic');
  public static TABS: HTMLCanvasElement = <HTMLCanvasElement>document.querySelector('.statistic-list > .nav');
  public static TEXT_AREA_SELECTOR: string = '#reports-values-statistic textarea';
  public show: boolean;
  public $reportsSelect: any;
  public tinyMCE: any;
  private reportsFilesManager: ReportsFilesManager;

  constructor() {
    this.reportsFilesManager = new ReportsFilesManager();
    this.show = false;

    this
      .initTinyMCE()
      .then((editors) => {
        this.tinyMCE = editors[0];

        $(ReportsValuesStatistic.TABS).on('shown.bs.tab', this.onToggleTab);
        $(ReportsValuesStatistic.TABS).on('hidden.bs.tab', this.onToggleTab);

        this.renderReport();
        this.subscribeUpdate();
      })
      .catch((err: Error) => {
        console.error(err);
      });
  }

  private initTinyMCE(): Promise<any> {
    let fontSizeFormats = '';

    for(let i = 8; i <= 48; i += 2) {
      fontSizeFormats += `${i}pt `;
    }

    return (<any>window).tinymce.init({
      selector: ReportsValuesStatistic.TEXT_AREA_SELECTOR,
      height: 500,
      theme: 'modern',
      language: 'uk_UA',
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking table contextmenu directionality',
        'emoticons paste textcolor colorpicker textpattern imagetools codesample toc help'
      ],
      fontsize_formats: fontSizeFormats.trim(),
      toolbar1: 'undo redo | sizeselect | bold italic | fontselect | fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help'
    });
  }

  private onToggleTab = () => {
    const $typeSelector = $('#statistic-chart-type'),
      $dateSelectorWrapper = $('.datepicker-wrap');
    this.show = ReportsValuesStatistic.CONTAINER.classList.contains('active');

    if(this.show) {
      $typeSelector.selectpicker('hide');
      $dateSelectorWrapper.hide();
    } else {
      $typeSelector.selectpicker('show');
      $dateSelectorWrapper.show();
    }

    this.renderReportsSelect();
  };

  private renderReportsSelect = () => {
    let selected: string = null;

    if (this.$reportsSelect) {
      selected = this.$reportsSelect.val();
      this.$reportsSelect.remove();
      this.$reportsSelect = null;
    }
    if (!this.show) return;

    this.$reportsSelect = $(this.generateReportsSelectTpl(selected));
    $('#statistic .manage-bar').prepend(this.$reportsSelect);
    this.$reportsSelect
      .find('select')
      .selectpicker({
        multiple: false,
        noneSelectedText: 'Виберіть звіт'
      })
      .on('change', this.renderReport);
  };

  private generateReportsSelectTpl(selected: string): string {
    return [
      '<div class="col-md-3">',
      '<select>',
      this
        .reportsFilesManager
        .listSubject
        .getValue()
        .sort((a, b) => {
          if (!a.createdAt) return -1;
          if (!b.createdAt) return 1;
          if (a.createdAt > b.createdAt) return -1;
          if (a.createdAt < b.createdAt) return 1;
          return 0;
        })
        .map((report: IReportItemData) => `
          <option ${selected === report.fd ? 'selected' : ''} value="${report.fd}">
            ${report.name}
            ${!report.author ? '' : `
              <span class="badge badge-default">
                &nbsp-&nbsp${report.author}
              </span>
            `}
            ${!report.createdAt ? '' : `
              <span class="badge badge-default">
                &nbsp(${displayCreatedDate(report.createdAt)})
              </span>
            `}
          </option>`
        ),
      '</select>',
      '</div>'
    ].join('');
  }

  private subscribeUpdate() {
    this.reportsFilesManager.listSubject.subscribe(this.renderReportsSelect);
    this.reportsFilesManager.listSubject.subscribe(this.renderReport);
  }

  private renderReport = () => {
    this.tinyMCE.setContent(this.getComputedContent());
  };

  private getComputedContent(): string {
    const activeReport = this.getActiveReport(),
      activeContent = activeReport ? activeReport.body : '',
      style='text-align: center;font-weight: 700;background-color: #257cb6;color: #fff;',
      $activeContent = $(`<div>${activeContent}</div>`);

    $activeContent.find('tr').each((i, tr) => {
      const $tr = $(tr);
      let totals = {};

      $tr.find('td').each((i, td) => {
        const textContent = (td.textContent || '0').trim() || '0',
          val = parseFloat(textContent.replace(',', '.').replace(/\s+/g, '')),
          prefixMatch = textContent.match(/[^\d\s.,]+$/),
          prefix = prefixMatch ? prefixMatch[0] : '';

        if(isNaN(val)) {
          totals[prefix] = void 0;
          return;
        }
        isNaN(totals[prefix]) && (totals[prefix] = 0);
        totals[prefix] += val;
      });

      let $appendedTd = null;
      (<any>Object).keys(totals).forEach((prefix) => {
        const total = totals[prefix];
        if(!isNaN(total) && total !== null) {
          if(!$appendedTd) {
            $appendedTd = $(`<td style="${style}"></td>`);
            $tr.append($appendedTd);
          }
          $appendedTd.append(`<p>${prefix ? prefix + ' - ' : ''}${total}</p>`);
        }
      });
    });

    return $activeContent.html();
  }

  private getActiveReport(): IReportItemData {
    const selected: string = this.$reportsSelect && this.$reportsSelect.find('select').val();
    return this
      .reportsFilesManager
      .listSubject
      .getValue()
      .filter((report: IReportItemData) => report.fd === selected)[0] || null;
  }
}
