import * as $ from 'jquery';
import { Chart, ChartData } from 'chart.js';
import * as moment from 'moment';
import { groupBy, unionBy } from 'lodash';
import { generateColorHash, hexToRgb, RxEventEmitter } from '../utils';
import { IReportItemData } from '../ReportsFilesManager';

export class AuthorStatistic {
  public static CONTAINER: HTMLElement = <HTMLElement>document.querySelector('#detail-author-statistic');
  public static CANVAS_CONTAINER: HTMLCanvasElement = <HTMLCanvasElement>AuthorStatistic.CONTAINER.querySelector('canvas');
  public static TABS: HTMLCanvasElement = <HTMLCanvasElement>document.querySelector('.statistic-list > .nav');
  public reportsSubject: RxEventEmitter;
  public typeSubject: RxEventEmitter;
  public chart: Chart;
  public show: boolean;
  public $authorsSelect: any;
  private oldChartType: string;

  constructor(dateSubject: RxEventEmitter, typeSubject: RxEventEmitter) {
    this.reportsSubject = dateSubject;
    this.typeSubject = typeSubject;
    this.show = false;

    $(AuthorStatistic.TABS).on('shown.bs.tab', this.onToggleTab);
    $(AuthorStatistic.TABS).on('hidden.bs.tab', this.onToggleTab);
    this.renderChart();
    this.subscribeUpdate();
  }

  private onToggleTab = () => {
    const $typeSelector = $('#statistic-chart-type'),
      $typeOptions = $typeSelector.find('option[value!="line"]');
    this.show = AuthorStatistic.CONTAINER.classList.contains('active');

    if(this.show) {
      this.oldChartType = $typeSelector.val();
      $typeSelector.val('line');
      $typeOptions.attr('disabled', 'true');
    } else {
      $typeOptions.removeAttr('disabled');
      $typeSelector.val(this.oldChartType);
      this.oldChartType = null;
    }

    (<any>$typeSelector).selectpicker('refresh');
    this.renderAuthorsSelect();
  };

  private renderAuthorsSelect = () => {
    let selected: string[] = null;

    if (this.$authorsSelect) {
      selected = this.$authorsSelect.val();
      this.$authorsSelect.remove();
      this.$authorsSelect = null;
    }
    if (!this.show) return;

    this.$authorsSelect = $(this.generateAuthorsSelectTpl(selected));
    $('#statistic .manage-bar').append(this.$authorsSelect);
    this.$authorsSelect
      .find('select')
      .selectpicker({
        multiple: true,
        noneSelectedText: 'Виберіть працівника',
        selectedAllText: 'Вибрані всі працівники'
      })
      .on('change', this.renderChart);
  };

  private generateAuthorsSelectTpl(selected: string[]): string {
    Array.isArray(selected) || (selected = []);

    return [
      '<div class="col-md-3">',
      '<select multiple>',
      unionBy(this.reportsSubject.getValue(), (report: any) => report.author.toLowerCase().trim())
        .map((report: any) => {
          const author: string = report.author,
            val: string = author.toLowerCase().trim();
          if (selected.indexOf(val) >= 0) {
            return `<option selected value="${val}">${author}</option>`;
          }
          return `<option value="${val}">${author}</option>`;
        }),
      '</select>',
      '</div>'
    ].join('');
  }

  private subscribeUpdate() {
    this.reportsSubject.subscribe(this.renderChart);
    this.reportsSubject.subscribe(this.renderAuthorsSelect);
    this.typeSubject.subscribe(this.renderChart);
  }

  private renderChart = () => {
    if (this.chart) this.chart.destroy();


    this.chart = new Chart(AuthorStatistic.CANVAS_CONTAINER, {
      type: 'line',
      data: this.getChartData(),
      options: <any>{
        maintainAspectRatio: false,
        responsive: true,
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 50,
            bottom: 0
          }
        },
        scales: {
          xAxes: [{
            type: 'time',
            time: {
              displayFormats: {
                quarter: 'MMM YYYY'
              }
            },
            scaleLabel: {
              display: true
            }
          }]
        }
      }
    });
  };

  private getChartData(): ChartData {
    const selected: string[] = this.$authorsSelect ? this.$authorsSelect.find('select').val() : [],
      reportsGroups: any = groupBy(this.reportsSubject
          .getValue()
          .filter((report: any) => selected.indexOf(report.author.toLowerCase().trim()) >= 0)
        ,
        (report: IReportItemData) => report.author.trim().toLowerCase());

    return {
      datasets: (<any>Object).values(reportsGroups).map((reports: IReportItemData[]) => {
        const rgb: any = hexToRgb(generateColorHash(reports[0].author)),
          borderColor: string = `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, 1)`,
          bgColor: string = `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, 0)`,
          counts = {};

        reports.map((report: IReportItemData) => {
          const date = moment(report.createdAt).startOf('month').toISOString();
          counts[date] || (counts[date] = {
            x: date,
            y: 0
          });
          counts[date].y++;
        });

        return {
          borderColor: borderColor,
          backgroundColor: bgColor,
          label: reports[0].author,
          data: (<any>Object).values(counts)
        }
      })
    };
  }
}
