export function generateColorHash(author: string): string {
  let hash = 0,
    colour = '#';

  author = author.toLowerCase();
  for (let i = 0; i < author.length; i++) {
    hash = author.charCodeAt(i) + ((hash << 5) - hash);
  }

  for (let i = 0; i < 3; i++) {
    let value = (hash >> (i * 8)) & 0xFF;
    colour += ('00' + value.toString(16)).substr(-2);
  }

  return colour;
}

export function hexToRgb(hex) {
  const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  hex = hex.replace(shorthandRegex, function (m, r, g, b) {
    return r + r + g + g + b + b;
  });

  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null;
}


import { EventEmitter } from 'events';
export class RxEventEmitter extends EventEmitter {
  private static __keyEvent: string = '__ON_NEXT__';
  private _value: any;

  constructor(){
      super();
  }

  public getValue () {
    return this._value;
  }

  public onNext (data: any) {
    this._value = data;
    this.emit(RxEventEmitter.__keyEvent, data);
  }

  public subscribe(listener: Function) {
    listener(this.getValue());
    this.on(RxEventEmitter.__keyEvent, listener);
  }
}
