"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ReportsFilesManager_1 = require("./ReportsFilesManager");
const BASIC_TEMPLATE = `<p style=\"text-align: center;\"><span style=\"font-size: 12pt;\"><strong>Звіт про проведену роботу</strong></span></p>\n<p style=\"text-align: center;\"><span style=\"font-size: 12pt;\"><strong>головного спеціаліста віддділу ЕТН та ЯМП</strong></span></p>\n<p style=\"text-align: center;\"><span style=\"font-size: 12pt;\">&nbsp;</span></p>\n<table style=\"height: 86px; width: 100%; border-color: #000000;\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td style=\"width: 73px; text-align: center;\"><span style=\"font-size: 12pt;\"><strong>№ п/п</strong></span></td>\n<td style=\"width: 573px; text-align: center;\"><span style=\"font-size: 12pt;\"><strong>Найменування показника&nbsp;</strong></span></td>\n<td style=\"width: 159px; text-align: center;\"><span style=\"font-size: 12pt;\"><strong>Значення показника</strong></span></td>\n</tr>\n<tr>\n<td style=\"width: 73px; text-align: center;\"><span style=\"font-size: 12pt;\"><strong>&nbsp;1</strong></span></td>\n<td style=\"width: 573px; text-align: center;\"><span style=\"font-size: 12pt;\"><strong>2&nbsp;</strong></span></td>\n<td style=\"width: 159px; text-align: center;\"><span style=\"font-size: 12pt;\"><strong>3&nbsp;</strong></span></td>\n</tr>\n<tr>\n<td style=\"width: 73px; text-align: center;\"><span style=\"font-size: 12pt;\">&nbsp;1.</span></td>\n<td style=\"width: 573px;\"><span style=\"font-size: 12pt;\">&nbsp;</span></td>\n<td style=\"width: 159px;\"><span style=\"font-size: 12pt;\">&nbsp;</span></td>\n</tr>\n</tbody>\n</table>`;
class ReportCreator {
    constructor() {
        this.reportsFilesManager = new ReportsFilesManager_1.ReportsFilesManager();
        const reportAddBtn = document.querySelector(ReportCreator.REPORT_ADD_BUTTON_SELECTOR);
        this.onAddClick = this.onAddClick.bind(this);
        reportAddBtn.addEventListener('click', this.onAddClick, false);
    }
    onAddClick(e) {
        e.preventDefault();
        const reportsList = this.reportsFilesManager.listSubject.getValue(), reportNameInput = document.querySelector(ReportCreator.REPORT_NAME_INPUT_SELECTOR), inputValue = reportNameInput.value, reportName = inputValue ? inputValue : `Звіт ${reportsList.length}`, fd = ReportsFilesManager_1.ReportsFilesManager.getUniqueFd(), body = this.getBasicTemplate();
        this.reportsFilesManager.add({
            body: body,
            name: reportName,
            fd: fd,
            createdAt: Date.now(),
            author: ''
        });
    }
    getBasicTemplate() {
        const reportsList = this.reportsFilesManager.listSubject.getValue(), indexBasicTemplate = reportsList.findIndex((reportData) => reportData.name === ReportCreator.BASIC_TEMPLATE_NAME);
        if (indexBasicTemplate >= 0) {
            return reportsList[indexBasicTemplate].body;
        }
        else if (reportsList[0]) {
            reportsList[0].body;
        }
        this.createBasicTemplate();
        return BASIC_TEMPLATE;
    }
    createBasicTemplate() {
        this.reportsFilesManager.add({
            fd: `0${ReportsFilesManager_1.ReportsFilesManager.fileExt}`,
            body: BASIC_TEMPLATE,
            name: ReportCreator.BASIC_TEMPLATE_NAME,
            createdAt: 0,
            author: ''
        });
    }
}
ReportCreator.REPORT_NAME_INPUT_SELECTOR = '#reports .new-report-data .report-name input';
ReportCreator.REPORT_ADD_BUTTON_SELECTOR = '#reports .new-report-data .create-report button';
ReportCreator.BASIC_TEMPLATE_NAME = 'Базовий шаблон';
exports.ReportCreator = ReportCreator;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9zY3JpcHRzL1JlcG9ydENyZWF0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSwrREFBNkU7QUFDN0UsTUFBTSxjQUFjLEdBQUcsKy9DQUErL0MsQ0FBQztBQUd2aEQ7SUFPRTtRQUNFLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLHlDQUFtQixFQUFFLENBQUM7UUFFckQsTUFBTSxZQUFZLEdBQTZCLFFBQVEsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLDBCQUEwQixDQUFDLENBQUM7UUFDaEgsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM3QyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDakUsQ0FBQztJQUVPLFVBQVUsQ0FBQyxDQUFRO1FBQ3pCLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUVuQixNQUFNLFdBQVcsR0FBc0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsRUFDcEYsZUFBZSxHQUF1QyxRQUFRLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQywwQkFBMEIsQ0FBQyxFQUN0SCxVQUFVLEdBQVcsZUFBZSxDQUFDLEtBQUssRUFDMUMsVUFBVSxHQUFXLFVBQVUsR0FBRyxVQUFVLEdBQUcsUUFBUSxXQUFXLENBQUMsTUFBTSxFQUFFLEVBQzNFLEVBQUUsR0FBVyx5Q0FBbUIsQ0FBQyxXQUFXLEVBQUUsRUFDOUMsSUFBSSxHQUFXLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBRXpDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUM7WUFDM0IsSUFBSSxFQUFFLElBQUk7WUFDVixJQUFJLEVBQUUsVUFBVTtZQUNoQixFQUFFLEVBQUUsRUFBRTtZQUNOLFNBQVMsRUFBRSxJQUFJLENBQUMsR0FBRyxFQUFFO1lBQ3JCLE1BQU0sRUFBRSxFQUFFO1NBQ1gsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVNLGdCQUFnQjtRQUNyQixNQUFNLFdBQVcsR0FBc0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsRUFDcEYsa0JBQWtCLEdBQVcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQTJCLEtBQUssVUFBVSxDQUFDLElBQUksS0FBSyxhQUFhLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUU3SSxFQUFFLENBQUMsQ0FBQyxrQkFBa0IsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLE1BQU0sQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDOUMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzFCLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDdEIsQ0FBQztRQUVELElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQzNCLE1BQU0sQ0FBQyxjQUFjLENBQUM7SUFDeEIsQ0FBQztJQUVPLG1CQUFtQjtRQUN6QixJQUFJLENBQUMsbUJBQW1CLENBQUMsR0FBRyxDQUFDO1lBQzNCLEVBQUUsRUFBRSxJQUFJLHlDQUFtQixDQUFDLE9BQU8sRUFBRTtZQUNyQyxJQUFJLEVBQUUsY0FBYztZQUNwQixJQUFJLEVBQUUsYUFBYSxDQUFDLG1CQUFtQjtZQUN2QyxTQUFTLEVBQUUsQ0FBQztZQUNaLE1BQU0sRUFBRSxFQUFFO1NBQ1gsQ0FBQyxDQUFBO0lBQ0osQ0FBQzs7QUF2RGMsd0NBQTBCLEdBQVcsOENBQThDLENBQUM7QUFDcEYsd0NBQTBCLEdBQVcsaURBQWlELENBQUM7QUFDdkYsaUNBQW1CLEdBQVcsZ0JBQWdCLENBQUM7QUFIaEUsc0NBeURDIiwiZmlsZSI6IlJlcG9ydENyZWF0b3IuanMiLCJzb3VyY2VSb290IjoiLi9hcHAvc3JjL3NjcmlwdHMvIn0=
