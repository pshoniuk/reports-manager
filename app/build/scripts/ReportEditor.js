"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ReportsFilesManager_1 = require("./ReportsFilesManager");
const moment = require("moment");
class ReportEditor {
    constructor() {
        if (ReportEditor.instance)
            return ReportEditor.instance;
        ReportEditor.instance = this;
        this.reportsFilesManager = new ReportsFilesManager_1.ReportsFilesManager();
        this
            .initTinyMCE()
            .then((editors) => {
            this.tinyMCE = editors[0];
            this.bindManageActions();
        })
            .catch((err) => {
            console.error(err);
        });
    }
    initTinyMCE() {
        let fontSizeFormats = '';
        for (let i = 8; i <= 48; i += 2) {
            fontSizeFormats += `${i}pt `;
        }
        return window.tinymce.init({
            selector: ReportEditor.BODY_SELECTOR,
            height: 500,
            theme: 'modern',
            language: 'uk_UA',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking table contextmenu directionality',
                'emoticons paste textcolor colorpicker textpattern imagetools codesample toc help'
            ],
            fontsize_formats: fontSizeFormats.trim(),
            toolbar1: 'undo redo | sizeselect | bold italic | fontselect | fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help'
        });
    }
    bindManageActions() {
        const saveBtn = document.querySelector(ReportEditor.SAVE_SELECTOR);
        this.saveClick = this.saveClick.bind(this);
        saveBtn.addEventListener('click', this.saveClick, false);
        const cancelBtn = document.querySelector(ReportEditor.CANCEL_SELECTOR);
        this.cancelClick = this.cancelClick.bind(this);
        cancelBtn.addEventListener('click', this.cancelClick, false);
        const removeBtn = document.querySelector(ReportEditor.REMOVE_SELECTOR);
        this.removeClick = this.removeClick.bind(this);
        removeBtn.addEventListener('click', this.removeClick, false);
    }
    saveClick(e) {
        e.preventDefault();
        this.reportsFilesManager.update(Object.assign({}, this.editedReportData, { createdAt: +(new Date(document.getElementById('sdate').value)) || Date.now(), body: window.tinyMCE.activeEditor.getContent(), name: this.getNameInput().value, author: this.getAuthorInput().value }));
    }
    cancelClick(e) {
        e.preventDefault();
        this.editReport(this.editedReportData);
    }
    removeClick(e) {
        e.preventDefault();
        const editedReportData = this.editedReportData;
        this.editReport(null);
        this.reportsFilesManager.remove(editedReportData);
    }
    editReport(reportData) {
        this.editedReportData = reportData;
        if (!reportData)
            return this.hideEditor();
        this.showEditor();
        this.getNameInput().value = reportData.name;
        this.getAuthorInput().value = reportData.author;
        document.getElementById('sdate').value = moment(reportData.createdAt).format('YYYY-MM-DD');
        this.tinyMCE.setContent(reportData.body);
    }
    showEditor() {
        this.getContainer().style.display = '';
    }
    hideEditor() {
        this.getContainer().style.display = 'none';
    }
    getContainer() {
        return document.querySelector(ReportEditor.CONTAINER_SELECTOR);
    }
    getNameInput() {
        return document.querySelector(ReportEditor.NAME_SELECTOR);
    }
    getAuthorInput() {
        return document.querySelector(ReportEditor.AUTHOR_SELECTOR);
    }
}
ReportEditor.CONTAINER_SELECTOR = '#report-editor';
ReportEditor.NAME_SELECTOR = '#report-editor .report-name input';
ReportEditor.AUTHOR_SELECTOR = '#report-editor .report-author input';
ReportEditor.BODY_SELECTOR = '#report-editor .report-body textarea';
ReportEditor.SAVE_SELECTOR = '#report-editor .manage-buttons .save';
ReportEditor.CANCEL_SELECTOR = '#report-editor .manage-buttons .cancel';
ReportEditor.REMOVE_SELECTOR = '#report-editor .manage-buttons .remove';
exports.ReportEditor = ReportEditor;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9zY3JpcHRzL1JlcG9ydEVkaXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLCtEQUE2RTtBQUM3RSxpQ0FBaUM7QUFFakM7SUFjRTtRQUNFLEVBQUUsQ0FBQyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUM7WUFBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQztRQUN4RCxZQUFZLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUU3QixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSx5Q0FBbUIsRUFBRSxDQUFDO1FBQ3JELElBQUk7YUFDRCxXQUFXLEVBQUU7YUFDYixJQUFJLENBQUMsQ0FBQyxPQUFPO1lBQ1osSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDMUIsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFDM0IsQ0FBQyxDQUFDO2FBQ0QsS0FBSyxDQUFDLENBQUMsR0FBVTtZQUNoQixPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVPLFdBQVc7UUFDakIsSUFBSSxlQUFlLEdBQUcsRUFBRSxDQUFDO1FBRXpCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztZQUNoQyxlQUFlLElBQUksR0FBRyxDQUFDLEtBQUssQ0FBQztRQUMvQixDQUFDO1FBRUQsTUFBTSxDQUFPLE1BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQ2hDLFFBQVEsRUFBRSxZQUFZLENBQUMsYUFBYTtZQUNwQyxNQUFNLEVBQUUsR0FBRztZQUNYLEtBQUssRUFBRSxRQUFRO1lBQ2YsUUFBUSxFQUFFLE9BQU87WUFDakIsT0FBTyxFQUFFO2dCQUNQLDZFQUE2RTtnQkFDN0Usa0VBQWtFO2dCQUNsRSxtRUFBbUU7Z0JBQ25FLGtGQUFrRjthQUNuRjtZQUNELGdCQUFnQixFQUFFLGVBQWUsQ0FBQyxJQUFJLEVBQUU7WUFDeEMsUUFBUSxFQUFFLGtLQUFrSztZQUM1SyxRQUFRLEVBQUUsdUVBQXVFO1NBQ2xGLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxpQkFBaUI7UUFDdkIsTUFBTSxPQUFPLEdBQXlDLFFBQVEsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3pHLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0MsT0FBTyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBRXpELE1BQU0sU0FBUyxHQUF5QyxRQUFRLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUM3RyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9DLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUU3RCxNQUFNLFNBQVMsR0FBeUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDN0csSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVPLFNBQVMsQ0FBQyxDQUFRO1FBQ3hCLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUVuQixJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxtQkFDMUIsSUFBSSxDQUFDLGdCQUFnQixJQUN4QixTQUFTLEVBQUUsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFvQixRQUFRLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLEdBQUcsRUFBRSxFQUNoRyxJQUFJLEVBQVEsTUFBTyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFLEVBQ3JELElBQUksRUFBRSxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsS0FBSyxFQUMvQixNQUFNLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLEtBQUssSUFDbkMsQ0FBQTtJQUNKLENBQUM7SUFFTyxXQUFXLENBQUMsQ0FBUTtRQUMxQixDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7UUFFbkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRU8sV0FBVyxDQUFDLENBQVE7UUFDMUIsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRW5CLE1BQU0sZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1FBQy9DLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFTSxVQUFVLENBQUMsVUFBMkI7UUFDM0MsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFVBQVUsQ0FBQztRQUNuQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7UUFFMUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQztRQUM1QyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUM7UUFDN0IsUUFBUSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUUsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDL0csSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFTyxVQUFVO1FBQ2hCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztJQUN6QyxDQUFDO0lBRU8sVUFBVTtRQUNoQixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7SUFDN0MsQ0FBQztJQUVPLFlBQVk7UUFDbEIsTUFBTSxDQUFjLFFBQVEsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDOUUsQ0FBQztJQUVPLFlBQVk7UUFDbEIsTUFBTSxDQUFtQixRQUFRLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUM5RSxDQUFDO0lBRU8sY0FBYztRQUNwQixNQUFNLENBQW1CLFFBQVEsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQ2hGLENBQUM7O0FBekhhLCtCQUFrQixHQUFXLGdCQUFnQixDQUFDO0FBQzlDLDBCQUFhLEdBQVcsbUNBQW1DLENBQUM7QUFDNUQsNEJBQWUsR0FBVyxxQ0FBcUMsQ0FBQztBQUNoRSwwQkFBYSxHQUFXLHNDQUFzQyxDQUFDO0FBQy9ELDBCQUFhLEdBQVcsc0NBQXNDLENBQUM7QUFDL0QsNEJBQWUsR0FBVyx3Q0FBd0MsQ0FBQztBQUNuRSw0QkFBZSxHQUFXLHdDQUF3QyxDQUFDO0FBUm5GLG9DQTRIQyIsImZpbGUiOiJSZXBvcnRFZGl0b3IuanMiLCJzb3VyY2VSb290IjoiLi9hcHAvc3JjL3NjcmlwdHMvIn0=
