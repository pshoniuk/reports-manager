"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const $ = require("jquery");
const chart_js_1 = require("chart.js");
const moment = require("moment");
const lodash_1 = require("lodash");
const utils_1 = require("../utils");
class AuthorStatistic {
    constructor(dateSubject, typeSubject) {
        this.onToggleTab = () => {
            const $typeSelector = $('#statistic-chart-type'), $typeOptions = $typeSelector.find('option[value!="line"]');
            this.show = AuthorStatistic.CONTAINER.classList.contains('active');
            if (this.show) {
                this.oldChartType = $typeSelector.val();
                $typeSelector.val('line');
                $typeOptions.attr('disabled', 'true');
            }
            else {
                $typeOptions.removeAttr('disabled');
                $typeSelector.val(this.oldChartType);
                this.oldChartType = null;
            }
            $typeSelector.selectpicker('refresh');
            this.renderAuthorsSelect();
        };
        this.renderAuthorsSelect = () => {
            let selected = null;
            if (this.$authorsSelect) {
                selected = this.$authorsSelect.val();
                this.$authorsSelect.remove();
                this.$authorsSelect = null;
            }
            if (!this.show)
                return;
            this.$authorsSelect = $(this.generateAuthorsSelectTpl(selected));
            $('#statistic .manage-bar').append(this.$authorsSelect);
            this.$authorsSelect
                .find('select')
                .selectpicker({
                multiple: true,
                noneSelectedText: 'Виберіть працівника',
                selectedAllText: 'Вибрані всі працівники'
            })
                .on('change', this.renderChart);
        };
        this.renderChart = () => {
            if (this.chart)
                this.chart.destroy();
            this.chart = new chart_js_1.Chart(AuthorStatistic.CANVAS_CONTAINER, {
                type: 'line',
                data: this.getChartData(),
                options: {
                    maintainAspectRatio: false,
                    responsive: true,
                    layout: {
                        padding: {
                            left: 0,
                            right: 0,
                            top: 50,
                            bottom: 0
                        }
                    },
                    scales: {
                        xAxes: [{
                                type: 'time',
                                time: {
                                    displayFormats: {
                                        quarter: 'MMM YYYY'
                                    }
                                },
                                scaleLabel: {
                                    display: true
                                }
                            }]
                    }
                }
            });
        };
        this.reportsSubject = dateSubject;
        this.typeSubject = typeSubject;
        this.show = false;
        $(AuthorStatistic.TABS).on('shown.bs.tab', this.onToggleTab);
        $(AuthorStatistic.TABS).on('hidden.bs.tab', this.onToggleTab);
        this.renderChart();
        this.subscribeUpdate();
    }
    generateAuthorsSelectTpl(selected) {
        Array.isArray(selected) || (selected = []);
        return [
            '<div class="col-md-3">',
            '<select multiple>',
            lodash_1.unionBy(this.reportsSubject.getValue(), (report) => report.author.toLowerCase().trim())
                .map((report) => {
                const author = report.author, val = author.toLowerCase().trim();
                if (selected.indexOf(val) >= 0) {
                    return `<option selected value="${val}">${author}</option>`;
                }
                return `<option value="${val}">${author}</option>`;
            }),
            '</select>',
            '</div>'
        ].join('');
    }
    subscribeUpdate() {
        this.reportsSubject.subscribe(this.renderChart);
        this.reportsSubject.subscribe(this.renderAuthorsSelect);
        this.typeSubject.subscribe(this.renderChart);
    }
    getChartData() {
        const selected = this.$authorsSelect ? this.$authorsSelect.find('select').val() : [], reportsGroups = lodash_1.groupBy(this.reportsSubject
            .getValue()
            .filter((report) => selected.indexOf(report.author.toLowerCase().trim()) >= 0), (report) => report.author.trim().toLowerCase());
        return {
            datasets: Object.values(reportsGroups).map((reports) => {
                const rgb = utils_1.hexToRgb(utils_1.generateColorHash(reports[0].author)), borderColor = `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, 1)`, bgColor = `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, 0)`, counts = {};
                reports.map((report) => {
                    const date = moment(report.createdAt).startOf('month').toISOString();
                    counts[date] || (counts[date] = {
                        x: date,
                        y: 0
                    });
                    counts[date].y++;
                });
                return {
                    borderColor: borderColor,
                    backgroundColor: bgColor,
                    label: reports[0].author,
                    data: Object.values(counts)
                };
            })
        };
    }
}
AuthorStatistic.CONTAINER = document.querySelector('#detail-author-statistic');
AuthorStatistic.CANVAS_CONTAINER = AuthorStatistic.CONTAINER.querySelector('canvas');
AuthorStatistic.TABS = document.querySelector('.statistic-list > .nav');
exports.AuthorStatistic = AuthorStatistic;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9zY3JpcHRzL3N0YXRpc3RpYy9BdXRob3JTdGF0aXN0aWMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw0QkFBNEI7QUFDNUIsdUNBQTRDO0FBQzVDLGlDQUFpQztBQUNqQyxtQ0FBMEM7QUFDMUMsb0NBQXVFO0FBR3ZFO0lBV0UsWUFBWSxXQUEyQixFQUFFLFdBQTJCO1FBVzVELGdCQUFXLEdBQUc7WUFDcEIsTUFBTSxhQUFhLEdBQUcsQ0FBQyxDQUFDLHVCQUF1QixDQUFDLEVBQzlDLFlBQVksR0FBRyxhQUFhLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUM7WUFDN0QsSUFBSSxDQUFDLElBQUksR0FBRyxlQUFlLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7WUFFbkUsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ2IsSUFBSSxDQUFDLFlBQVksR0FBRyxhQUFhLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hDLGFBQWEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzFCLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ3hDLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixZQUFZLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNwQyxhQUFhLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDckMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7WUFDM0IsQ0FBQztZQUVLLGFBQWMsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDN0IsQ0FBQyxDQUFDO1FBRU0sd0JBQW1CLEdBQUc7WUFDNUIsSUFBSSxRQUFRLEdBQWEsSUFBSSxDQUFDO1lBRTlCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDckMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDN0IsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7WUFDN0IsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFBQyxNQUFNLENBQUM7WUFFdkIsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDakUsQ0FBQyxDQUFDLHdCQUF3QixDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUN4RCxJQUFJLENBQUMsY0FBYztpQkFDaEIsSUFBSSxDQUFDLFFBQVEsQ0FBQztpQkFDZCxZQUFZLENBQUM7Z0JBQ1osUUFBUSxFQUFFLElBQUk7Z0JBQ2QsZ0JBQWdCLEVBQUUscUJBQXFCO2dCQUN2QyxlQUFlLEVBQUUsd0JBQXdCO2FBQzFDLENBQUM7aUJBQ0QsRUFBRSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDcEMsQ0FBQyxDQUFDO1FBNEJNLGdCQUFXLEdBQUc7WUFDcEIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztnQkFBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBR3JDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxnQkFBSyxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDdkQsSUFBSSxFQUFFLE1BQU07Z0JBQ1osSUFBSSxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQ3pCLE9BQU8sRUFBTztvQkFDWixtQkFBbUIsRUFBRSxLQUFLO29CQUMxQixVQUFVLEVBQUUsSUFBSTtvQkFDaEIsTUFBTSxFQUFFO3dCQUNOLE9BQU8sRUFBRTs0QkFDUCxJQUFJLEVBQUUsQ0FBQzs0QkFDUCxLQUFLLEVBQUUsQ0FBQzs0QkFDUixHQUFHLEVBQUUsRUFBRTs0QkFDUCxNQUFNLEVBQUUsQ0FBQzt5QkFDVjtxQkFDRjtvQkFDRCxNQUFNLEVBQUU7d0JBQ04sS0FBSyxFQUFFLENBQUM7Z0NBQ04sSUFBSSxFQUFFLE1BQU07Z0NBQ1osSUFBSSxFQUFFO29DQUNKLGNBQWMsRUFBRTt3Q0FDZCxPQUFPLEVBQUUsVUFBVTtxQ0FDcEI7aUNBQ0Y7Z0NBQ0QsVUFBVSxFQUFFO29DQUNWLE9BQU8sRUFBRSxJQUFJO2lDQUNkOzZCQUNGLENBQUM7cUJBQ0g7aUJBQ0Y7YUFDRixDQUFDLENBQUM7UUFDTCxDQUFDLENBQUM7UUE5R0EsSUFBSSxDQUFDLGNBQWMsR0FBRyxXQUFXLENBQUM7UUFDbEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7UUFDL0IsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7UUFFbEIsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM3RCxDQUFDLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzlELElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDekIsQ0FBQztJQTJDTyx3QkFBd0IsQ0FBQyxRQUFrQjtRQUNqRCxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBRTNDLE1BQU0sQ0FBQztZQUNMLHdCQUF3QjtZQUN4QixtQkFBbUI7WUFDbkIsZ0JBQU8sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsTUFBVyxLQUFLLE1BQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUM7aUJBQ3pGLEdBQUcsQ0FBQyxDQUFDLE1BQVc7Z0JBQ2YsTUFBTSxNQUFNLEdBQVcsTUFBTSxDQUFDLE1BQU0sRUFDbEMsR0FBRyxHQUFXLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDNUMsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMvQixNQUFNLENBQUMsMkJBQTJCLEdBQUcsS0FBSyxNQUFNLFdBQVcsQ0FBQztnQkFDOUQsQ0FBQztnQkFDRCxNQUFNLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxNQUFNLFdBQVcsQ0FBQztZQUNyRCxDQUFDLENBQUM7WUFDSixXQUFXO1lBQ1gsUUFBUTtTQUNULENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQUVPLGVBQWU7UUFDckIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBcUNPLFlBQVk7UUFDbEIsTUFBTSxRQUFRLEdBQWEsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQzVGLGFBQWEsR0FBUSxnQkFBTyxDQUFDLElBQUksQ0FBQyxjQUFjO2FBQzNDLFFBQVEsRUFBRTthQUNWLE1BQU0sQ0FBQyxDQUFDLE1BQVcsS0FBSyxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFFckYsQ0FBQyxNQUF1QixLQUFLLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztRQUVyRSxNQUFNLENBQUM7WUFDTCxRQUFRLEVBQVEsTUFBTyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUEwQjtnQkFDM0UsTUFBTSxHQUFHLEdBQVEsZ0JBQVEsQ0FBQyx5QkFBaUIsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsRUFDN0QsV0FBVyxHQUFXLFFBQVEsR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLE1BQU0sRUFDN0QsT0FBTyxHQUFXLFFBQVEsR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLE1BQU0sRUFDekQsTUFBTSxHQUFHLEVBQUUsQ0FBQztnQkFFZCxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBdUI7b0JBQ2xDLE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUNyRSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUc7d0JBQzlCLENBQUMsRUFBRSxJQUFJO3dCQUNQLENBQUMsRUFBRSxDQUFDO3FCQUNMLENBQUMsQ0FBQztvQkFDSCxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ25CLENBQUMsQ0FBQyxDQUFDO2dCQUVILE1BQU0sQ0FBQztvQkFDTCxXQUFXLEVBQUUsV0FBVztvQkFDeEIsZUFBZSxFQUFFLE9BQU87b0JBQ3hCLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTTtvQkFDeEIsSUFBSSxFQUFRLE1BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO2lCQUNuQyxDQUFBO1lBQ0gsQ0FBQyxDQUFDO1NBQ0gsQ0FBQztJQUNKLENBQUM7O0FBM0phLHlCQUFTLEdBQTZCLFFBQVEsQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUMsQ0FBQztBQUN6RixnQ0FBZ0IsR0FBeUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7QUFDM0csb0JBQUksR0FBeUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO0FBSDlHLDBDQTZKQyIsImZpbGUiOiJzdGF0aXN0aWMvQXV0aG9yU3RhdGlzdGljLmpzIiwic291cmNlUm9vdCI6Ii4vYXBwL3NyYy9zY3JpcHRzLyJ9
