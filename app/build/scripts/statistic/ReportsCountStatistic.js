"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chart_js_1 = require("chart.js");
const lodash_1 = require("lodash");
const utils_1 = require("../utils");
class AuthorStatistic {
    constructor(dateSubject, typeSubject) {
        this.renderChart = () => {
            if (this.chart)
                this.chart.destroy();
            this.chart = new chart_js_1.Chart(AuthorStatistic.CANVAS_CONTAINER, {
                type: this.typeSubject.getValue() || 'bar',
                data: this.getChartData(),
                options: {
                    maintainAspectRatio: false,
                    responsive: true,
                    layout: {
                        padding: {
                            left: 0,
                            right: 0,
                            top: 50,
                            bottom: 0
                        }
                    }
                }
            });
        };
        this.reportsSubject = dateSubject;
        this.typeSubject = typeSubject;
        this.renderChart();
        this.subscribeUpdate();
    }
    subscribeUpdate() {
        this.reportsSubject.subscribe(this.renderChart);
        this.typeSubject.subscribe(this.renderChart);
    }
    getChartData() {
        const reports = this.reportsSubject.getValue(), reportsGroups = lodash_1.groupBy(reports, (report) => report.author.toLowerCase().trim()), reportsGroupsValues = Object.values(reportsGroups), labels = [
            'Всього',
            ...reportsGroupsValues.map((reports) => reports[0].author)
        ], data = [
            reports.length,
            ...reportsGroupsValues.map((reports) => reports.length)
        ], backgroundColor = labels.map(utils_1.generateColorHash).map((hex) => {
            const rgb = utils_1.hexToRgb(hex);
            return `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, 0.2)`;
        });
        return {
            labels,
            datasets: [{
                    label: 'Кількість поданих звітів',
                    backgroundColor,
                    data
                }]
        };
    }
}
AuthorStatistic.CONTAINER = document.querySelector('#author-statistic');
AuthorStatistic.CANVAS_CONTAINER = AuthorStatistic.CONTAINER.querySelector('canvas');
exports.AuthorStatistic = AuthorStatistic;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9zY3JpcHRzL3N0YXRpc3RpYy9SZXBvcnRzQ291bnRTdGF0aXN0aWMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSx1Q0FBNEM7QUFDNUMsbUNBQWlDO0FBQ2pDLG9DQUF1RTtBQUd2RTtJQU9FLFlBQVksV0FBMkIsRUFBRSxXQUEyQjtRQWE1RCxnQkFBVyxHQUFHO1lBQ3BCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7Z0JBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUNwQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksZ0JBQUssQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ3ZELElBQUksRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxJQUFJLEtBQUs7Z0JBQzFDLElBQUksRUFBRSxJQUFJLENBQUMsWUFBWSxFQUFFO2dCQUN6QixPQUFPLEVBQU87b0JBQ1osbUJBQW1CLEVBQUUsS0FBSztvQkFDMUIsVUFBVSxFQUFFLElBQUk7b0JBQ2hCLE1BQU0sRUFBRTt3QkFDTixPQUFPLEVBQUU7NEJBQ1AsSUFBSSxFQUFFLENBQUM7NEJBQ1AsS0FBSyxFQUFFLENBQUM7NEJBQ1IsR0FBRyxFQUFFLEVBQUU7NEJBQ1AsTUFBTSxFQUFFLENBQUM7eUJBQ1Y7cUJBQ0Y7aUJBQ0Y7YUFDRixDQUFDLENBQUM7UUFDTCxDQUFDLENBQUM7UUE5QkEsSUFBSSxDQUFDLGNBQWMsR0FBRyxXQUFXLENBQUM7UUFDbEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7UUFFL0IsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUN6QixDQUFDO0lBRU8sZUFBZTtRQUNyQixJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFzQk8sWUFBWTtRQUNsQixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRSxFQUM1QyxhQUFhLEdBQVEsZ0JBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxNQUF1QixLQUFLLE1BQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUMsRUFDdEcsbUJBQW1CLEdBQThCLE1BQU8sQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEVBQzlFLE1BQU0sR0FBRztZQUNQLFFBQVE7WUFDUixHQUFHLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQTBCLEtBQUssT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztTQUM5RSxFQUNELElBQUksR0FBRztZQUNMLE9BQU8sQ0FBQyxNQUFNO1lBQ2QsR0FBRyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUEwQixLQUFLLE9BQU8sQ0FBQyxNQUFNLENBQUM7U0FDM0UsRUFDRCxlQUFlLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyx5QkFBaUIsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUc7WUFDdEQsTUFBTSxHQUFHLEdBQUcsZ0JBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMxQixNQUFNLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDO1FBQ25ELENBQUMsQ0FBQyxDQUFDO1FBRUwsTUFBTSxDQUFDO1lBQ0wsTUFBTTtZQUNOLFFBQVEsRUFBRSxDQUFDO29CQUNULEtBQUssRUFBRSwwQkFBMEI7b0JBQ2pDLGVBQWU7b0JBQ2YsSUFBSTtpQkFDTCxDQUFDO1NBQ0gsQ0FBQztJQUNKLENBQUM7O0FBaEVhLHlCQUFTLEdBQTZCLFFBQVEsQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUMsQ0FBQztBQUNsRixnQ0FBZ0IsR0FBeUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7QUFGM0gsMENBa0VDIiwiZmlsZSI6InN0YXRpc3RpYy9SZXBvcnRzQ291bnRTdGF0aXN0aWMuanMiLCJzb3VyY2VSb290IjoiLi9hcHAvc3JjL3NjcmlwdHMvIn0=
