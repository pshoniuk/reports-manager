"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moment = require("moment");
const path = require("path");
const fs = require("fs");
const $ = require("jquery");
const ReportsFilesManager_1 = require("./ReportsFilesManager");
const utils_1 = require("./utils");
class Statistic {
    constructor() {
        this.updateReportsSubject = () => {
            this.reportsSubject.onNext(this.getReportsByDateRange(this.dateSubject.getValue()));
        };
        this.controllers = [];
        this.reportsFilesManager = new ReportsFilesManager_1.ReportsFilesManager();
        this.dateSubject = new utils_1.RxEventEmitter();
        this.typeSubject = new utils_1.RxEventEmitter();
        this.reportsSubject = new utils_1.RxEventEmitter();
        this.reportsFilesManager.listSubject.subscribe(this.updateReportsSubject);
        this.dateSubject.subscribe(this.updateReportsSubject);
        this.initDatePicker();
        this.initChartTypeSelector();
        this.connectControllers();
    }
    initDatePicker() {
        $.fn.datepicker.language['uk'] = {
            days: ['Неділя', 'Понеділок', 'Вівторок', 'Середа', 'Четвер', 'П\'ятница', 'Субота'],
            daysShort: ['Нед', 'Пон', 'Вів', 'Сер', 'Чет', 'П\'ят', 'Суб'],
            daysMin: ['Нд', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            months: ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'],
            monthsShort: ['Січ', 'Лют', 'Бер', 'Кві', 'Тра', 'Чер', 'Лип', 'Сер', 'Вер', 'Жов', 'Лис', 'Гру'],
            today: 'Сьогодні',
            clear: 'Очистити',
            dateFormat: 'dd.mm.yyyy',
            timeFormat: 'hh:ii',
            firstDay: 1
        };
        $(Statistic.DATE_PICKER_SELECTOR)
            .datepicker({
            range: true,
            language: 'uk',
            maxDate: moment().endOf('day').toDate(),
            onSelect: (formattedDates, simpleDates) => {
                simpleDates || (simpleDates = []);
                simpleDates[0] || (simpleDates[0] = simpleDates[1] || Date.now());
                simpleDates[1] || (simpleDates[1] = simpleDates[0] || Date.now());
                const dateRange = simpleDates.map((date) => moment(date).endOf('day').toDate());
                this.dateSubject.onNext(dateRange);
            }
        })
            .data('datepicker')
            .selectDate([
            moment().startOf('month').toDate(),
            moment().endOf('day').toDate()
        ]);
    }
    initChartTypeSelector() {
        $(Statistic.TYPE_SELECT_SELECTOR).on('change', (e) => {
            this.typeSubject.onNext($(e.target).val());
        });
    }
    connectControllers() {
        const controllersPaths = fs.readdirSync(path.join(__dirname, 'statistic')) || [], ControllersModules = controllersPaths.map((controllerPath) => {
            return require(path.join(__dirname, 'statistic', controllerPath));
        });
        ControllersModules.forEach((ControllersModule) => {
            Object.values(ControllersModule)
                .filter((exportItem) => typeof exportItem === 'function')
                .forEach((Controller) => {
                this.controllers.push(new Controller(this.reportsSubject, this.typeSubject));
            });
        });
    }
    getReportsByDateRange(datesRange) {
        if (!datesRange)
            return [];
        const timestampRange = datesRange.map(date => +(new Date(date))).sort();
        return this.reportsFilesManager.listSubject.getValue().filter((report) => {
            const createdAt = +report.createdAt;
            return createdAt >= timestampRange[0] && createdAt <= timestampRange[1];
        });
    }
}
Statistic.DATE_PICKER_SELECTOR = '#statistic-date-picker';
Statistic.TYPE_SELECT_SELECTOR = '#statistic-chart-type';
exports.Statistic = Statistic;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9zY3JpcHRzL1N0YXRpc3RpYy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLGlDQUFpQztBQUNqQyw2QkFBNkI7QUFDN0IseUJBQXlCO0FBQ3pCLDRCQUE0QjtBQUM1QiwrREFBNkU7QUFDN0UsbUNBQXlDO0FBRXpDO0lBU0U7UUFpRFEseUJBQW9CLEdBQUc7WUFDN0IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3RGLENBQUMsQ0FBQztRQWxEQSxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSx5Q0FBbUIsRUFBRSxDQUFDO1FBQ3JELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxzQkFBYyxFQUFFLENBQUM7UUFDeEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLHNCQUFjLEVBQUUsQ0FBQztRQUN4QyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksc0JBQWMsRUFBRSxDQUFDO1FBRTNDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQzFFLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBRXRELElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQztRQUM3QixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBRU8sY0FBYztRQUNwQixDQUFDLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUc7WUFDL0IsSUFBSSxFQUFFLENBQUMsUUFBUSxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsUUFBUSxDQUFDO1lBQ3BGLFNBQVMsRUFBRSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssQ0FBQztZQUM5RCxPQUFPLEVBQUUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUM7WUFDbkQsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxTQUFTLENBQUM7WUFDM0ksV0FBVyxFQUFFLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUM7WUFDakcsS0FBSyxFQUFFLFVBQVU7WUFDakIsS0FBSyxFQUFFLFVBQVU7WUFDakIsVUFBVSxFQUFFLFlBQVk7WUFDeEIsVUFBVSxFQUFFLE9BQU87WUFDbkIsUUFBUSxFQUFFLENBQUM7U0FDWixDQUFDO1FBRUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBRTthQUNyQyxVQUFVLENBQUM7WUFDVixLQUFLLEVBQUUsSUFBSTtZQUNYLFFBQVEsRUFBRSxJQUFJO1lBQ2QsT0FBTyxFQUFFLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEVBQUU7WUFDdkMsUUFBUSxFQUFFLENBQUMsY0FBYyxFQUFFLFdBQVc7Z0JBQ3BDLFdBQVcsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUMsQ0FBQztnQkFDbEMsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztnQkFDbEUsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztnQkFDbEUsTUFBTSxTQUFTLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksS0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7Z0JBQ2hGLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3JDLENBQUM7U0FDRixDQUFDO2FBQ0QsSUFBSSxDQUFDLFlBQVksQ0FBQzthQUNsQixVQUFVLENBQUM7WUFDVixNQUFNLEVBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTSxFQUFFO1lBQ2xDLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEVBQUU7U0FDL0IsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQU1PLHFCQUFxQjtRQUMzQixDQUFDLENBQUMsU0FBUyxDQUFDLG9CQUFvQixDQUFDLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQVE7WUFDdEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQzdDLENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUVPLGtCQUFrQjtRQUN4QixNQUFNLGdCQUFnQixHQUFhLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsV0FBVyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQ3hGLGtCQUFrQixHQUFpQixnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxjQUFjO1lBQ3JFLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsV0FBVyxFQUFFLGNBQWMsQ0FBQyxDQUFDLENBQUM7UUFDcEUsQ0FBQyxDQUFDLENBQUM7UUFFTCxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxpQkFBaUI7WUFDckMsTUFBTyxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQztpQkFDcEMsTUFBTSxDQUFDLENBQUMsVUFBVSxLQUFLLE9BQU8sVUFBVSxLQUFLLFVBQVUsQ0FBQztpQkFDeEQsT0FBTyxDQUFDLENBQUMsVUFBVTtnQkFDbEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztZQUMvRSxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLHFCQUFxQixDQUFDLFVBQVU7UUFDdEMsRUFBRSxDQUFBLENBQUMsQ0FBQyxVQUFVLENBQUM7WUFBQyxNQUFNLENBQUMsRUFBRSxDQUFDO1FBQzFCLE1BQU0sY0FBYyxHQUFHLFVBQVUsQ0FBQyxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7UUFFeEUsTUFBTSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBdUI7WUFDcEYsTUFBTSxTQUFTLEdBQUcsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDO1lBQ3BDLE1BQU0sQ0FBQyxTQUFTLElBQUksY0FBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLFNBQVMsSUFBSSxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDMUUsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOztBQTFGYSw4QkFBb0IsR0FBRyx3QkFBd0IsQ0FBQztBQUNoRCw4QkFBb0IsR0FBRyx1QkFBdUIsQ0FBQztBQUYvRCw4QkE0RkMiLCJmaWxlIjoiU3RhdGlzdGljLmpzIiwic291cmNlUm9vdCI6Ii4vYXBwL3NyYy9zY3JpcHRzLyJ9
