"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jQuery = require("jquery");
const moment = require("moment");
const utils_1 = require("./utils");
const ReportsFilesManager_1 = require("./ReportsFilesManager");
const ReportEditor_1 = require("./ReportEditor");
function displayCreatedDate(timestamp) {
    moment.locale('uk');
    return moment(timestamp).format('ll');
}
exports.displayCreatedDate = displayCreatedDate;
class ReportsList {
    constructor() {
        this.reportsFilesManager = new ReportsFilesManager_1.ReportsFilesManager();
        this.reportEditor = new ReportEditor_1.ReportEditor();
        this.render = this.render.bind(this);
        this.reportsFilesManager.listSubject.subscribe(this.render);
        this.onSelectItem = this.onSelectItem.bind(this);
        jQuery(this.getContainer()).on('click', '.list-group-item', this.onSelectItem);
        this.onRefreshClick = this.onRefreshClick.bind(this);
        jQuery(ReportsList.REFRESH_SELECTOR).on('click', this.onRefreshClick);
    }
    render(reportsList) {
        const container = this.getContainer(), $activeItem = jQuery('.list-group-item.active'), activeFD = $activeItem.length && $activeItem.data('fd');
        let newContent = '';
        reportsList
            .sort((a, b) => {
            if (!a.createdAt)
                return -1;
            if (!b.createdAt)
                return 1;
            if (a.createdAt > b.createdAt)
                return -1;
            if (a.createdAt < b.createdAt)
                return 1;
            return 0;
        })
            .forEach((reportData, i) => {
            newContent += `
        <a href="#" 
          class="list-group-item"
          data-id="${i}"
          data-fd="${reportData.fd}"
        >
          <div class="author-label" style="background-color: ${utils_1.generateColorHash(reportData.author)}"></div>
          <div class="content">
            <div class="name">
              ${reportData.name}
            </div>
            <div class="time">
              ${!reportData.createdAt ? '' : `
              <span class="badge badge-default">
                ${displayCreatedDate(reportData.createdAt)}
              </span>
              `}
            </div>
          </div>
        </a>`;
        });
        container.innerHTML = newContent;
        if (activeFD) {
            const $futureActiveItem = jQuery(this.getContainer()).find(`[data-fd="${activeFD}"]`);
            $futureActiveItem.addClass('active');
        }
    }
    getContainer() {
        return document.querySelector(ReportsList.LIST_SELECTOR);
    }
    onSelectItem(e) {
        e.preventDefault();
        const $item = jQuery(e.target).closest('.list-group-item'), id = $item.data('id');
        jQuery(this.getContainer())
            .find('.list-group-item')
            .not($item)
            .removeClass('active');
        $item.toggleClass('active');
        const reportData = $item.hasClass('active') ?
            this.reportsFilesManager.listSubject.getValue()[id] : null;
        this.reportEditor.editReport(reportData);
    }
    onRefreshClick(e) {
        e.preventDefault();
        this.reportsFilesManager.updateList();
    }
}
ReportsList.LIST_SELECTOR = '#reports .reports-list';
ReportsList.REFRESH_SELECTOR = '#reports .refresh-list';
exports.ReportsList = ReportsList;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9zY3JpcHRzL1JlcG9ydHNMaXN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsaUNBQWlDO0FBQ2pDLGlDQUFpQztBQUNqQyxtQ0FBNEM7QUFDNUMsK0RBQTZFO0FBQzdFLGlEQUE4QztBQUU5Qyw0QkFBbUMsU0FBaUI7SUFDbEQsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNwQixNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN4QyxDQUFDO0FBSEQsZ0RBR0M7QUFFRDtJQU9FO1FBQ0UsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUkseUNBQW1CLEVBQUUsQ0FBQztRQUNyRCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksMkJBQVksRUFBRSxDQUFDO1FBRXZDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRTVELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakQsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRS9FLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckQsTUFBTSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ3hFLENBQUM7SUFFTSxNQUFNLENBQUMsV0FBOEI7UUFDMUMsTUFBTSxTQUFTLEdBQWdCLElBQUksQ0FBQyxZQUFZLEVBQUUsRUFDaEQsV0FBVyxHQUFHLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxFQUMvQyxRQUFRLEdBQUcsV0FBVyxDQUFDLE1BQU0sSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRTFELElBQUksVUFBVSxHQUFXLEVBQUUsQ0FBQztRQUU1QixXQUFXO2FBQ1IsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDVCxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7Z0JBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztnQkFBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQzNCLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQztnQkFBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDO2dCQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDeEMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQzthQUNELE9BQU8sQ0FBQyxDQUFDLFVBQTJCLEVBQUUsQ0FBQztZQUN0QyxVQUFVLElBQUk7OztxQkFHRCxDQUFDO3FCQUNELFVBQVUsQ0FBQyxFQUFFOzsrREFFNkIseUJBQWlCLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQzs7O2dCQUduRixVQUFVLENBQUMsSUFBSTs7O2dCQUdmLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxFQUFFLEdBQUc7O2tCQUUzQixrQkFBa0IsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDOztlQUUzQzs7O2FBR0YsQ0FBQztRQUNSLENBQUMsQ0FBQyxDQUFDO1FBRUwsU0FBUyxDQUFDLFNBQVMsR0FBRyxVQUFVLENBQUM7UUFFakMsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUNiLE1BQU0saUJBQWlCLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLFFBQVEsSUFBSSxDQUFDLENBQUM7WUFDdEYsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZDLENBQUM7SUFDSCxDQUFDO0lBRU0sWUFBWTtRQUNqQixNQUFNLENBQWMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDeEUsQ0FBQztJQUVPLFlBQVksQ0FBQyxDQUFRO1FBQzNCLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNuQixNQUFNLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxFQUN4RCxFQUFFLEdBQVcsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUVoQyxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO2FBQ3hCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQzthQUN4QixHQUFHLENBQUMsS0FBSyxDQUFDO2FBQ1YsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRXpCLEtBQUssQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDNUIsTUFBTSxVQUFVLEdBQW9CLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO1lBQzFELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBRTdELElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFTyxjQUFjLENBQUMsQ0FBUTtRQUM3QixDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQ3hDLENBQUM7O0FBMUZhLHlCQUFhLEdBQVcsd0JBQXdCLENBQUM7QUFDakQsNEJBQWdCLEdBQVcsd0JBQXdCLENBQUM7QUFGcEUsa0NBNEZDIiwiZmlsZSI6IlJlcG9ydHNMaXN0LmpzIiwic291cmNlUm9vdCI6Ii4vYXBwL3NyYy9zY3JpcHRzLyJ9
