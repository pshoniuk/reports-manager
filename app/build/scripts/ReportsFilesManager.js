"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require('fs');
const path = require('path');
const homedir = require('homedir');
const utils_1 = require("./utils");
class ReportsFilesManager {
    constructor() {
        if (ReportsFilesManager.instance)
            return ReportsFilesManager.instance;
        ReportsFilesManager.instance = this;
        this.storeDir = path.resolve(ReportsFilesManager.storeDir);
        this.listSubject = new utils_1.RxEventEmitter();
        this.updateList();
    }
    static getUniqueFd() {
        return `${Date.now()}_${ReportsFilesManager.uid++}${ReportsFilesManager.fileExt}`;
    }
    updateList() {
        this.prepareStoreDir();
        const filesList = (fs.readdirSync(this.storeDir) || [])
            .filter((fileName) => path.extname(fileName) === ReportsFilesManager.fileExt)
            .map((fileName) => ({
            fd: fileName,
            file: fs.readFileSync(path.join(this.storeDir, fileName), 'utf8')
        }))
            .map((fileData) => {
            try {
                const jsonData = JSON.parse(fileData.file);
                return Object.assign({ fd: fileData.fd }, jsonData, { author: jsonData.author || 'Невизначено' });
            }
            catch (e) {
                return null;
            }
        });
        this.listSubject.onNext(filesList);
    }
    add(reportData) {
        const writeData = Object.assign({}, reportData);
        delete writeData.fd;
        this.prepareStoreDir();
        fs.writeFileSync(path.join(this.storeDir, reportData.fd), JSON.stringify(writeData));
        this.updateList();
    }
    update(reportData) {
        this.add(reportData);
    }
    remove(reportData) {
        this.prepareStoreDir();
        fs.unlinkSync(path.join(this.storeDir, reportData.fd));
        this.updateList();
    }
    prepareStoreDir() {
        if (!fs.existsSync(this.storeDir)) {
            fs.mkdirSync(this.storeDir);
        }
    }
}
ReportsFilesManager.storeDir = path.join(homedir(), 'reports');
ReportsFilesManager.fileExt = '.dat';
ReportsFilesManager.uid = 0;
exports.ReportsFilesManager = ReportsFilesManager;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9zY3JpcHRzL1JlcG9ydHNGaWxlc01hbmFnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxNQUFNLEVBQUUsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDekIsTUFBTSxJQUFJLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQzdCLE1BQU0sT0FBTyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztBQUNuQyxtQ0FBeUM7QUFZekM7SUFZRTtRQUNFLEVBQUUsQ0FBQSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQztZQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUM7UUFFckUsbUJBQW1CLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNwQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLHNCQUFjLEVBQUUsQ0FBQztRQUN4QyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQWZNLE1BQU0sQ0FBQyxXQUFXO1FBQ3ZCLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsR0FBRyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNwRixDQUFDO0lBZU0sVUFBVTtRQUNmLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUV2QixNQUFNLFNBQVMsR0FBc0IsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDdkUsTUFBTSxDQUFDLENBQUMsUUFBZ0IsS0FBSyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxLQUFLLG1CQUFtQixDQUFDLE9BQU8sQ0FBQzthQUNwRixHQUFHLENBQUMsQ0FBQyxRQUFnQixLQUFLLENBQUM7WUFDMUIsRUFBRSxFQUFFLFFBQVE7WUFDWixJQUFJLEVBQUUsRUFBRSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsUUFBUSxDQUFDLEVBQUUsTUFBTSxDQUFDO1NBQ2xFLENBQUMsQ0FBQzthQUNGLEdBQUcsQ0FBQyxDQUFDLFFBQW9DO1lBQ3hDLElBQUksQ0FBQztnQkFDSCxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDM0MsTUFBTSxDQUFDLGdCQUNMLEVBQUUsRUFBRSxRQUFRLENBQUMsRUFBRSxJQUNaLFFBQVEsSUFDWCxNQUFNLEVBQUUsUUFBUSxDQUFDLE1BQU0sSUFBSSxhQUFhLEdBQ3pDLENBQUE7WUFDSCxDQUFDO1lBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDWCxNQUFNLENBQUMsSUFBSSxDQUFDO1lBQ2QsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUwsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVNLEdBQUcsQ0FBQyxVQUEyQjtRQUNwQyxNQUFNLFNBQVMscUJBQU8sVUFBVSxDQUFDLENBQUM7UUFFbEMsT0FBTyxTQUFTLENBQUMsRUFBRSxDQUFDO1FBRXBCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN2QixFQUFFLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ3JGLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBRU0sTUFBTSxDQUFDLFVBQTJCO1FBQ3ZDLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDdkIsQ0FBQztJQUVNLE1BQU0sQ0FBQyxVQUEyQjtRQUN2QyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdkIsRUFBRSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQ3BCLENBQUM7SUFFTyxlQUFlO1FBQ3JCLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzlCLENBQUM7SUFDSCxDQUFDOztBQXJFYSw0QkFBUSxHQUFXLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUUsU0FBUyxDQUFDLENBQUM7QUFDbkQsMkJBQU8sR0FBVyxNQUFNLENBQUM7QUFDekIsdUJBQUcsR0FBVyxDQUFDLENBQUM7QUFIaEMsa0RBdUVDIiwiZmlsZSI6IlJlcG9ydHNGaWxlc01hbmFnZXIuanMiLCJzb3VyY2VSb290IjoiLi9hcHAvc3JjL3NjcmlwdHMvIn0=
